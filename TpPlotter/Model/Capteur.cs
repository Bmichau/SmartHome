﻿using System;
using System.Collections.Generic;

public class Capteur
{
    public string Nom { get; set; }
    public string Description { get; set; }
    public string Lieu { get; set; }
    public Grandeur Grandeur { get; set; }

    public int Seuil { get; set; }
    public List<Mesure> MesuresList { get; set; }
    public int Taille { get { return MesuresList.Count; } }
    public override string ToString()
    {
        return Nom;
    }
    public Capteur(string unNom)
    {
        Nom = unNom;
        MesuresList = new List<Mesure>();
    }

    public void AjouteMesure(Mesure m)
    {
        MesuresList.Add(m);
    }

}