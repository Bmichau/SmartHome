﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TpPlotter.Model
{
    public class JourEvent
    {
        public DateTime dateFete { get; set; }
        public DateTime heureDebutFete { get; set; }
        public DateTime heureFinFete { get; set; }

        public JourEvent(DateTime uneDateFete, DateTime uneHeureDebutFete, DateTime uneHeureFinFete)
        {
            dateFete = uneDateFete;
            heureDebutFete = uneHeureDebutFete;
            heureFinFete = uneHeureFinFete; 
        }

    }
}
