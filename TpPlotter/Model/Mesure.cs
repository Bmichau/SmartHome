﻿using System;

public class Mesure
{

    public DateTime Date { get; set; }
    public double Valeur { get; set; }
    public string Id { get; set; }
    public Mesure(string uneId, DateTime uneDate, double uneValeur)
    {
        Id = uneId;
        Date = uneDate;
        Valeur = uneValeur;
    }

    public override string ToString()
    {
        return Convert.ToString(Valeur);
    }
}
