﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TpPlotter.Model
{
    public class JourAbsence
    {

        public string date { get; set; }
        public bool isMissing { get; set; }

        public JourAbsence()
        {
        }

        public JourAbsence(string uneDate, bool IsMissing)
        {
            date = uneDate;
            isMissing = IsMissing;
        }
    }
}
