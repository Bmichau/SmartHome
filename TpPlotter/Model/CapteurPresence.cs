﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TpPlotter.Model
{
    public class CapteurPresence
    {
        public string nomCapteur { get; set; }
        public List<JourAbsence> joursPresence { get; set; }


        public CapteurPresence()
        {
        }
        public CapteurPresence(string unNomCapteur, List<JourAbsence> JoursPresence)
        {
            nomCapteur = unNomCapteur;
            joursPresence = JoursPresence;
        }

    }
}
