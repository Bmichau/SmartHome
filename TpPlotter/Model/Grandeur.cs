﻿using System;

public class Grandeur
{
    public string Nom { get; set; }
    public string Unité { get; set; }
    public string Abréviation { get; set; }
    public Grandeur(string unNom, string uneUnité, string uneAbrev) { Nom = unNom; Unité = uneUnité; Abréviation = uneAbrev; }
}
