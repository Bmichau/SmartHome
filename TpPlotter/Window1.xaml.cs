﻿using MindFusion.Scheduling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TpPlotter.Model;

namespace TpPlotter
{
    /// <summary>
    /// Logique d'interaction pour Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1(List<CapteurPresence> val, List<JourEvent> jourFetes, List<JourEvent> jourFemmeH)
        {
            InitializeComponent();
            DataContext = App.ViewModelCalendar;
            calendar.ItemSettings.Size = 40;
            // recuperation des appointment pour les vacs
            List<Appointment> listeAptToAdd = App.ViewModelCalendar.ajouterEventCalendar(val);
            // recuperation des appointment pour les fetes
            List<Appointment> listeAptFeteToAdd = App.ViewModelCalendar.remplirJourFete(jourFetes);

            // recuperation des appointment pour les jours de passage de la femme de menage
            List<Appointment> listeAptFemmeHToAdd = App.ViewModelCalendar.remplirJourFemmeH(jourFemmeH);

            // on parcours les listes et on ajout dans le calendar
            foreach (Appointment ap in listeAptToAdd)
            {
                calendar.Schedule.Items.Add(ap);
            }
            foreach (Appointment apt in listeAptFeteToAdd)
            {
                calendar.Schedule.Items.Add(apt);
            }
            
            foreach (Appointment apt in listeAptFemmeHToAdd)
            {
                calendar.Schedule.Items.Add(apt);
            }
            
        }

        // gestion du filtre
        private void btFiltrer_Click(object sender, RoutedEventArgs e)
        {
            List<Item> appointments = calendar.Schedule.Items.ToList();
            if (cbAbsFamille.IsChecked == false)
            {
                App.ViewModelCalendar.cacherAppointment("Abs", appointments);
            }
            else
            {
                App.ViewModelCalendar.afficherAppointment("Abs", appointments);
            }
            if (cbVacs.IsChecked == false)
            {
                App.ViewModelCalendar.cacherAppointment("Vacs", appointments);
            }
            else
            {
                App.ViewModelCalendar.afficherAppointment("Vacs", appointments);
            }

            if (cbFetes.IsChecked == false)
            {
                App.ViewModelCalendar.cacherAppointment("Fête", appointments);
            }
            else
            {
                App.ViewModelCalendar.afficherAppointment("Fête", appointments);
            }
            if (cbFemmeM.IsChecked == false)
            {
                App.ViewModelCalendar.cacherAppointment("menage", appointments);
            }
            else
            {
                App.ViewModelCalendar.afficherAppointment("menage", appointments);
            }

        }
    }
}
