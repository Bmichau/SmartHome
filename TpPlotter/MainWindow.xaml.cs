using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using TpPlotter.Model;


namespace TpPlotter
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<Capteur> contentAllFiles;

        static List<CapteurPresence> res;

        static List<JourEvent> resJFete;

        static List<JourEvent> resjourFemmeM;

        static Capteur listeCapteurSalle;

        static CapteurPresence valueSalle;


        public MainWindow()
        {
            InitializeComponent();
            DataContext = App.ViewModelSmartHome;
            loadData();
            selectDate.SelectedDate = new DateTime(2014, 01, 31);
           
        }

        
        private async void loadData()
        {
            await Task.Run(() => loadAllData());
        }

        private void loadAllData()
        {
            Dispatcher.Invoke((() =>
            {
                label1.Content = "";
                analyseDonnees.Content = "Chargement des donn�es  en cours ...";
            }));
            //recuperation du contenu de toutes les fichiers .dt
            contentAllFiles = App.ViewModelSmartHome.LireTousLesFichierDT("../../ecole/netatmo/");
            // on donne l'acc�s au bouton "tracer courbe" qu'une fois que le chargement de toute les donn�es est fini
            Dispatcher.Invoke((() =>
            {
                analyseDonnees.Content = "Chargement du plot termin� \nChargement du calendrier...";
                button_tracer_courbe.IsEnabled = true;
            }));
            // recuperation des jours de vacances
            res = lancerInfosVac();
            // recuperation des jours de fetes
            resJFete = lancerInfoFetes();

            //recuperaton des jours femme de menage
            resjourFemmeM = lancerInfoFemmeM();

            // lorsque qu'on a r�cup�r� toutes les infos pour le calendrier on affiche le bouton calendrier
            Dispatcher.Invoke((() =>
            {
                if(analyseDonnees.Content.ToString() != "")
                {
                    analyseDonnees.Content = "Chargement du calendrier termin� ";
                }else
                {
                    analyseDonnees.Content += "Chargement du calendrier termin� ";
                }
                
                button_show_calendar.IsEnabled = true;
            }));
        }

        public static List<CapteurPresence> lancerInfosVac()
        {
            List<CapteurPresence> listRes = new List<CapteurPresence>();

            Capteur listeCapteurCA = contentAllFiles.Where(x => x.Nom.Equals("co2chambrealain")).First();

            Capteur listeCapteurCB= contentAllFiles.Where(x => x.Nom.Equals("co2chambrebeatrice")).First();

            listeCapteurSalle = contentAllFiles.Where(x => x.Nom.Equals("co2salle")).First();

            // dictionnaire de valeurs pour chambre alain
            CapteurPresence valueCA = App.ViewModelSmartHome.jourVacances(listeCapteurCA);

            // dictionnaire de valeurs pour chambre b�atrice
            CapteurPresence valueCB = App.ViewModelSmartHome.jourVacances(listeCapteurCB);

            // dictionnaire de valeurs pour la salle.
            valueSalle = App.ViewModelSmartHome.jourVacances(listeCapteurSalle);


            listRes.Add(valueCA);
            listRes.Add(valueCB);
            listRes.Add(valueSalle);

            return listRes;
        }

        public static List<JourEvent> lancerInfoFetes()
        {
            List<JourEvent> resJFete = App.ViewModelSmartHome.jourFetes(listeCapteurSalle);
            return resJFete;
        }

        public static List<JourEvent> lancerInfoFemmeM()
        {
            Capteur listeCapteurNoise = contentAllFiles.Where(a => a.Nom.Equals("noisesalle")).First();
            resjourFemmeM = App.ViewModelSmartHome.jourFemmeDeMenage(valueSalle, listeCapteurNoise);

            return resjourFemmeM;
        }
        // traitement au cliquer sur lancer courbe
        private void BtnWriteCurve_OnClick(object sender, RoutedEventArgs e)
        {
            // r�cup�ration du capteur et de ces mesures pour la date s�l�ctionn�e
            Capteur capteur = App.ViewModelSmartHome.LireFichierDT(contentAllFiles, new Capteur(selectCapteur.Text), selectDate.Text);
            if(capteur !=null)
            {
                IEnumerable<Capteur> capteurs = SmartHomeViewModel.LireFichierXml("../../ecole/capteurs.xtim");
                string lieu = "";
                int seuil = 0;
                // on �crit l'abscisse avec le nom de la grandeur et l'unit�
                foreach (Capteur sensor in capteurs)
                {
                    if (sensor.Nom == selectCapteur.Text)
                    {
                        SmartHomeViewModel.myPlot.Axes.ElementAt(1).Title = sensor.Grandeur.Nom + " (" + sensor.Grandeur.Abr�viation + ")";
                        lieu = sensor.Lieu;
                        seuil = sensor.Seuil;
                    }
                }
                // on trace la courbe avec les donn�es fournies
                App.ViewModelSmartHome.tracerCourbe(capteur.MesuresList);

                // si un capteur de co2 est demand� alors on fait le traitement pour afficher l'analyse de donn�es pr�sence de la personne
                if (selectCapteur.Text.Contains("co2"))
                {
                    List<DataPoint> listDataPoint = new List<DataPoint>();
                    foreach (LineSeries ls in SmartHomeViewModel.myPlot.Series)
                    {
                        listDataPoint = ls.Points;
                    }
                    char delimiter = ',';
                    string[] analyse = App.ViewModelSmartHome.afficherHeuresPresences(listDataPoint, seuil).Split(delimiter);
                    label1.Content = "Analyse de donn�es : ";
                    analyseDonnees.Content += "\nLe " + selectDate.Text + " Une personne ou plusieurs �tait pr�sente dans " + lieu + " \nde " + analyse[0] + " � " + analyse[1];
                }
            } 

        }

        /**
         * Fonction de clique sur calendrier on appele la fenetre du calendrier 
         *
         **/
        private void BtnShowCalendar_OnClick(object sender, RoutedEventArgs e)
        {
            Window1 wd = new Window1(res, resJFete, resjourFemmeM);
            wd.Show();
        }

        /**
         * Fonction de r�nitialisation du plot lorsque l'on clique sur r�initialiser
         * 
         **/
       private void btnRebootPlot_OnClick(object sender, RoutedEventArgs e)
        {
            App.ViewModelSmartHome.resetPlot();
            
             analyseDonnees.Content = "";
            
            
        }

       
    }
}  
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   