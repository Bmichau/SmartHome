﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TpPlotter
{
    public partial class App : Application
    {
        public static SmartHomeViewModel ViewModelSmartHome { get; set; }
        public static CalendarViewModel ViewModelCalendar { get; set; }

        public App() {
            ViewModelSmartHome = new SmartHomeViewModel();
            ViewModelCalendar = new CalendarViewModel();
        }


    }
}
