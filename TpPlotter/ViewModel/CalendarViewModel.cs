﻿using MindFusion.Scheduling;
using MindFusion.Scheduling.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TpPlotter.Model;

namespace TpPlotter
{
    public class CalendarViewModel
    {
        Calendar calendar { get; set; }
        

        public CalendarViewModel()
        {

        }


        /**
         * Fonction qui permet d'ajouter les evenements au calendar en lui passant une liste d'appointments
         * 
         **/
        public List<Appointment> ajouterEventCalendar(List<CapteurPresence> val)
        {
            
            // recuperation des trois dictionnaires des capteurs
            List<JourAbsence> listeJoursVacsCA = val.ElementAt(0).joursPresence;

            List<JourAbsence> listeJoursVacsCB = val.ElementAt(1).joursPresence;

            List<JourAbsence> listeJoursVacsSalle = val.ElementAt(2).joursPresence;


            // on prend les jours ou il y a true donc que le capteur na rien dectecté de la journée
            List<String> dateVacsCA = listeJoursVacsCA.Where(x => x.isMissing.Equals(true)).Select(x => x.date).ToList();
            List<String> dateVacsCB = listeJoursVacsCB.Where(x => x.isMissing.Equals(true)).Select(x => x.date).ToList();
            List<String> dateVacsSalle = listeJoursVacsSalle.Where(x => x.isMissing.Equals(true)).Select(x => x.date).ToList();

            // List<String> dateFeteSalle = listeJoursVacsSalle.Where(x => x.Value.Equals(true)).Select(x => x.Key).ToList();

            // fusion des listes
            List<String> dateVacs = dateVacsCA.Intersect(dateVacsCB).ToList();

            List<String> dateVacsFinale = dateVacs.Intersect(dateVacsSalle).ToList();

            List<Appointment> listeApt = new List<Appointment>();
            int a = 0;
            string legend = "";
            DateTime dateVac;
            Appointment app;

            // on parcours la liste des dates ou la famille est abs
            for (int i = 0; i < dateVacsFinale.Count; i++)
            {
                if (i < dateVacsFinale.Count - 1 & i != 0)
                {
                    // on compare si la valeur de la date prochaine est égale a celle actuelle + 1
                    if (DateTime.Parse(dateVacsFinale.ElementAt(i)).AddDays(-1) == DateTime.Parse(dateVacsFinale.ElementAt(i - 1)) ||
                        DateTime.Parse(dateVacsFinale.ElementAt(i + 1)) == DateTime.Parse(dateVacsFinale.ElementAt(i)).AddDays(1))
                    {
                        a++;
                    }
                    else
                    {
                        a = 0;
                    }
                }

                // a partir de plus de 3j d'abs on considere que la famille est en vacs
                if (a >= 3)
                {
                    // on rempli avec vacs la ou il est nécessaire
                    for (int j = i; j > 0; j--)
                    {

                        legend = "Vacs";
                        dateVac = DateTime.Parse(dateVacsFinale.ElementAt(j));
                        app = new Appointment();
                        app.HeaderText = legend;
                        
                        app.StartTime = dateVac;
                        app.EndTime = dateVac;
                        
                        if (!listeApt.Select(x=>x.StartTime).ToList().Contains(dateVac))
                        {
                            listeApt.Add(app);
                        }

                    }


                }
                // sinon si on est en dessous de 3j d'abs on remplie avc famille abs
                else if (a == 0)
                {
                    legend = "Famille Abs";
                    // ajout des events dans le calendrier
                    dateVac = DateTime.Parse(dateVacsFinale.ElementAt(i));
                    app = new Appointment();
                    app.HeaderText = legend;
                    app.StartTime = dateVac;
                    app.EndTime = dateVac;
                    listeApt.Add(app);
                }

            }
            return listeApt;
        }

        /**
         * Fonction qui permet de remplir le calendrier avec les jours de fetes
         * 
         **/
        public List<Appointment> remplirJourFete(List<JourEvent> listeJFete)
        {
            Appointment app;
            List<Appointment> listeAptFete = new List<Appointment>();
            foreach (JourEvent jf in listeJFete)
            {
                if (jf.heureDebutFete.Hour != jf.heureFinFete.Hour)
                {
                    app = new Appointment();
                    app.StartTime = jf.dateFete;
                    app.EndTime = jf.dateFete;
                    app.HeaderText = "Fête de " + jf.heureDebutFete.Hour + "h\n à " + jf.heureFinFete.Hour + "h";

                    listeAptFete.Add(app);
                }
            }
            return listeAptFete;
        }

        /**
         * Fonction qui permet de remplir le calendrier avec les jours de fetes
         * 
         **/
        public List<Appointment> remplirJourFemmeH(List<JourEvent> listeJFemmeH)
        {
            Appointment app;
            List<Appointment> listeAptJFemmeH = new List<Appointment>();
            foreach (JourEvent jf in listeJFemmeH)
            {
                if(jf.heureDebutFete.Hour != jf.heureFinFete.Hour)
                {
                    app = new Appointment();
                    app.StartTime = jf.dateFete;
                    app.EndTime = jf.dateFete;
                    app.HeaderText = "Femme de menage de " + jf.heureDebutFete.Hour + "h" + jf.heureDebutFete.Minute + "à " + jf.heureFinFete.Hour + "h" + jf.heureDebutFete.Minute;

                    listeAptJFemmeH.Add(app);
                }
               
            }
            return listeAptJFemmeH;
        }

        /**
         * Fonction qui permet de cacher tout les appointments d'une type d'appointment
         * 
         **/
        public void cacherAppointment(String typeApt, List<Item> listeApt)
        {
            foreach(Item it in listeApt)
            {
                if (it.HeaderText.Contains(typeApt))
                {
                    it.Visible = false;
                }
            }
        }

        /**
         * Fonction qui permet d'afficher tout les appointments d'une type d'appointment
         * 
         **/
        public void afficherAppointment(String typeApt, List<Item> listeApt)
        {
            foreach (Item it in listeApt)
            {
                if (it.HeaderText.Contains(typeApt))
                {
                    it.Visible = true;
                }
            }
        }


    }
}
