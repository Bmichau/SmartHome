﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using TpPlotter.Model;

namespace TpPlotter
{
    
    public class SmartHomeViewModel
    {
        public string nomApplication { get; set; }
        public List<Capteur> CapteursList { get; set; }

        public static IEnumerable<Capteur> capteurs;

        public DateTime date;

        public DateTime selectDate { get; set; }

        public string AnalyseFirstLine { get; set; }
        public String labelTest { get; set; }
        public static string lineColor { get; set; }
        public ComboBox selectCapteur { get; set; }
        public static PlotModel myPlot { get; set; }

        public string textLblLoad { get; set; }
        //public DateTime dateSelected { get; set; }
        public SmartHomeViewModel()
        {
           
          //  analyseFirstLine = "mlj";
            myPlot = new PlotModel { Title = "Courbe" };
            LinearAxis abscisse = new LinearAxis();
            abscisse.Title = "Heure";
            abscisse.Position = AxisPosition.Bottom;
            //Génération Axe Y Valeurs capteurs
            LinearAxis ordonnee = new LinearAxis();
            ordonnee.Title = "Valeur Capteurs";
            ordonnee.Position = AxisPosition.Left;
            //Ajout des axes au PlotModel
            myPlot.Axes.Add(abscisse);
            myPlot.Axes.Add(ordonnee);
            nomApplication = "Application SmartHome";
            CapteursList = new List<Capteur>();
            
            capteurs = LireFichierXml("../../ecole/capteurs.xtim");
            foreach (Capteur capteur in capteurs)
            {
                CapteursList.Add(capteur);
               
            }


        }
        
       
        public static IEnumerable<Capteur> LireFichierXml(string strFichier)
        {
            XDocument xdoc = XDocument.Load(strFichier);
           
            XElement xdocattributs = xdoc.Root;
            if (xdocattributs != null)
            {
                    IEnumerable<XElement> xCapteurList = xdocattributs.Elements("capteur");
                    foreach (XElement xCapteur in xCapteurList)
                    {
                        int seuil = 0;
                        string unNom = xCapteur.Element("id").Value;
                        if(unNom.Contains("co2") && !unNom.Contains("co2petitemaison"))
                        {
                            var xSeuils = xCapteur.Element("seuils");
                            var xSeuil = xSeuils.Element("seuil");
                            seuil = Int32.Parse(xSeuil.Attribute("valeur").Value);
                        }
                        var unCapteur = new Capteur(unNom) { Description = xCapteur.Element("description").Value, Lieu = xCapteur.Element("lieu").Value, Seuil = seuil };
                        var xGrandeur = xCapteur.Element("grandeur");
                        var nomGrand = xGrandeur.Attribute("nom").Value;
                        var unitéGrand = xGrandeur.Attribute("unite").Value;
                        var abrevGrand = xGrandeur.Attribute("abreviation").Value;
                        unCapteur.Grandeur = new Grandeur(nomGrand, unitéGrand, abrevGrand);
                        yield return unCapteur;
                    }
            }
        }

        /**
         * Fonction qui permet de lire les données d'un capteur
         * 
         **/
        public Capteur LireFichierDT(List<Capteur> listeCapteurs, Capteur capteur, String dateSouhaitee)
        {
            
            Capteur unCapteur = null;
            Boolean isNamed = false;
            int i = 0;
            foreach (Capteur sensor in listeCapteurs)
            {
                i++;
                string capteurName = sensor.Nom;
                DateTime date = sensor.MesuresList.ElementAt(i).Date;

                // si on trouve le nom de capteur souhaité dans la lsite
                if (capteurName == capteur.ToString())
                {
                    
                    // recupération des mesures du capteur
                    foreach(Mesure mes in sensor.MesuresList)
                    {
                        // si on est la bonne date on recup les mesures
                        if(mes.Date.ToString("dd/MM/yyyy") == dateSouhaitee)
                        {
                            if(isNamed == false)
                            {
                                unCapteur = new Capteur(capteurName);
                                isNamed = true;
                            }
                            unCapteur.MesuresList.Add(mes);
                        }
                    }
                }
           }
            
           return unCapteur;
        }

        /**
         *Fontion qui permet de parcourir tout les fichiers et stocker les données dans une liste d'objet capteur
         * 
         **/
        public List<Capteur> LireTousLesFichierDT(string strRepertoire)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(strRepertoire);
            FileInfo[] tabloFichier = dirInfo.GetFiles("*.dt");
            List<Capteur> contenuFichiers = new List<Capteur>();
            
            String line;
            foreach (FileInfo unFichier in tabloFichier)
            {
                if (unFichier.Length != 0)
                {
                    StreamReader file = new StreamReader(strRepertoire + unFichier);
                    // parcours des lignes du fichiers
                    while ((line = file.ReadLine()) != null)
                    {

                        string s = line;
                        string strDateEntiere = s.Substring(1, 19);
                        string strDate = s.Substring(1, 10);
                        s = s.Substring(22);
                        int positionEspace = s.IndexOf(' ');
                        string strCapteur = s.Substring(0, positionEspace);
                        string strValeur = s.Substring(positionEspace + 1);
                        DateTime uneDate = DateTime.Parse(strDateEntiere);
                        double uneValeur = double.Parse(strValeur);
                        Mesure m = new Mesure(strCapteur, uneDate, uneValeur);
                        Capteur unCapteur = new Capteur(strCapteur);
                        Capteur sense = null;

                        // si le capteur est déja dans la liste
                        if (contenuFichiers.Any(str => str.Nom.Contains(strCapteur)))
                        {
                            // on ajoute la mesure a sa liste de mesures
                            sense = new Capteur(strCapteur);
                            sense = contenuFichiers.First(x => x.Nom == strCapteur);
                            sense.MesuresList.Add(m);
                        //sinon on créer un nouveau capteur et on ajoute la mesure à sa liste
                        }else{
                            unCapteur = new Capteur(strCapteur);
                            unCapteur.MesuresList.Add(m);
                            contenuFichiers.Add(unCapteur);

                        }
                       
                    }
                }

            }

            return contenuFichiers;
        }
        

        /**
         * Fonction qui trace la courbe sur oxyplot grace a une liste de mesures fournies en arg
         * 
         **/
        public void tracerCourbe(List<Mesure> mesures)
        {

            LineSeries data = new LineSeries();
           
            for(int i=0;i<mesures.Count;i++) 
            {
                data.Points.Add(new DataPoint(mesures[i].Date.TimeOfDay.TotalHours, mesures[i].Valeur));

            }
            myPlot.Series.Add(data);
            myPlot.InvalidatePlot(true);
          
        }

        /**
         * Fonction qui founit un string avec les heures de présences d'une personne séparées par une virgule
         * 
         **/
        public string afficherHeuresPresences(List<DataPoint> dataPoints, int seuil)
        {
            List<DataPoint> listDpFiltered = dataPoints.Where(x => x.Y > seuil).ToList();
            var item = listDpFiltered.OrderByDescending(x => x.Y).First();
            string analyse = TimeSpan.FromHours(listDpFiltered.ElementAt(0).X).ToString("h\\:mm") + "," + TimeSpan.FromHours(item.X).ToString("h\\:mm");
            return analyse;
        }

        /**
         * Fonction qui retourne un dictionnaire des jours de vacances avec la date en clé et un boolean : true si il y a abs de la famille
         *
         **/
        public CapteurPresence jourVacances(Capteur content)
        {
            List<Mesure> listeMesuresAll = new List<Mesure>();
            CapteurPresence jp = new CapteurPresence();
            jp.nomCapteur = content.Nom;
            //liste de ttes les mesures dun capteur 

            listeMesuresAll = content.MesuresList;
              
          
            // recuperation des mesures groupées par date 
            var groupeDateMesureList = listeMesuresAll
                             .GroupBy(x => x.Date.ToString("dd'/'MM'/'yyyy"))
                             .ToDictionary(gdc => gdc.Key, gdc => gdc.ToList());

            //liste des Date avec un boolean pr dire si tt les mesures sont en dessous de seuil = abs
            List<JourAbsence> listeAbs = new List<JourAbsence>();

            // on parcours tout le dictionnaire
            foreach (var test in groupeDateMesureList)
            {
                foreach (var hh in test.Value)
                {
                    // valeur du seuil de présence
                    if (hh.Valeur >= 450 && hh.Valeur <= 900)
                    {
                        if (!listeAbs.Any( x=> x.date.Contains(hh.Date.ToString("dd'/'MM'/'yyyy"))))
                        {
                            listeAbs.Add(new JourAbsence(hh.Date.ToString("dd'/'MM'/'yyyy"), false));
                        }
                    }
                    else if (hh.Valeur <450)
                    {
                        if (!listeAbs.Any(x => x.date.Contains(hh.Date.ToString("dd'/'MM'/'yyyy"))))
                        {
                            listeAbs.Add(new JourAbsence(hh.Date.ToString("dd'/'MM'/'yyyy"), true));
                        }
                    }
                }
            }
            jp.joursPresence = listeAbs;
            return jp;
        }

        /**
         * Fonction qui retourne une liste de JourFete en lui passant la totalité des données netatmo
         * 
         **/
        public List<JourEvent> jourFetes(Capteur capteur)
        {
            List<Mesure> listeMesuresAll = capteur.MesuresList;

            


            //liste des Date avec un boolean pr dire si tt les mesures sont en dessous de seuil = abs
            List<DateTime> listeJourFetes = new List<DateTime>();
            List<JourEvent> listeJourFete = new List<JourEvent>();
            // on parcours tout le dictionnaire
            listeJourFetes = listeMesuresAll.Where(x => x.Valeur > 1800).Select(a => a.Date).ToList();
            var groupeDateMesureList = listeJourFetes
                             .GroupBy(x => x.Date.ToString("dd'/'MM'/'yyyy"))
                             .ToDictionary(gdc => gdc.Key, gdc => gdc.ToList());

            foreach(var x in groupeDateMesureList)
            {
                listeJourFete.Add(new JourEvent(DateTime.Parse(x.Key), x.Value.ElementAt(0), x.Value.ElementAt(x.Value.Count-1)));
            }
            return listeJourFete;
        }

        /**
         * Fonction retournant les jours de passage de la femme de menage avec ses heures de passage. On part du principe qu'il y a présence de la femme de menage lorsque que le taux
         * de co2 correspond a une personne et qu'il a y a beaucoup de bruits dans la salle
         * 
         **/
        public List<JourEvent> jourFemmeDeMenage(CapteurPresence capteurCo2, Capteur capteurNoise)
        {
            
            // Récuperation des jours de présences dans la salle
            List<JourAbsence> listJourPres= capteurCo2.joursPresence.ToList().Where(x => x.isMissing.Equals(false)).ToList();

            // Récuperation des dates de présences dans la salle
            var listeDatePres = listJourPres.Select(x => x.date).ToList();

            
            List<JourEvent> listeJourFemmeM = new List<JourEvent>();

            // recuperation des mesures lorsque que la valeur du capteur de bruit est sup a 50
            var capteureNoiseSup = capteurNoise.MesuresList.Where(x => x.Valeur > 50).ToList();

            // Heure de passage de la femme de ménage
            var capteurHFemmeMenage = capteureNoiseSup.Where(x => x.Date.Hour > 08).Where(x => x.Date.Hour < 18).ToList();

            // Femme de menage ne passe que en semaine
            var capteurFemmeMenageWeek = capteurHFemmeMenage.Where(x => x.Date.DayOfWeek.ToString() != "Saturday").Where(x => x.Date.DayOfWeek.ToString() != "Sunday").ToList();

            // On groupe par jour
            var capteurFemmeMenageWeekByDay = capteurFemmeMenageWeek
                            .GroupBy(x => x.Date.ToString("dd'/'MM'/'yyyy"))
                            .ToDictionary(gdc => gdc.Key, gdc => gdc.ToList());

            // on recupere l'intervalle de passage de la femme de ménage
            foreach (var x in capteurFemmeMenageWeekByDay)
            {
                if(x.Value.ElementAt(0).Date != x.Value.ElementAt(x.Value.Count - 1).Date && listeDatePres.Contains(x.Value.ElementAt(0).Date.ToString("dd'/'MM'/'yyyy"))){ 
                    listeJourFemmeM.Add(new JourEvent(DateTime.Parse(x.Key), x.Value.ElementAt(0).Date, x.Value.ElementAt(x.Value.Count - 1).Date));
                }
            }
            
            return listeJourFemmeM;
        }

        /**
         * Fonction qui permet de rénitialise les courbes
         * 
         **/
        public void resetPlot()
        {
            myPlot.Series.Clear();
            foreach (var axis in myPlot.Axes)
            {
                axis.Reset();
            }
            myPlot.ResetAllAxes();
            myPlot.InvalidatePlot(true);
        }


    }
}
